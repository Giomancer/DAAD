# "Cloak of Darkness"

### [Original](https://www.ifwiki.org/Cloak_of_Darkness)

  * Author: Roger Firth
  * Publisher: n/a
  * Release: n/a
  * System: n/a
  * Platform(s): n/a

##### Info

  * Implementor: Gio
  * System: DAAD
  * Version: 0.2.0

##### Description
This is an implementation of Roger Firth's "Cloak of Darkness" IF-language comparison specification in the DAAD system (a descendant of The Quill and PAW).

As of v0.2.0, it isn't pretty, but I wrote it as I learning the system, so I likely made poor or inefficient choices.

##### Usage
The recommended method of usage is to compile `cloak.dsf` with [DAAD-Ready](https://www.ngpaws.com/daadready/).

As of v0.2.0, there is a (GNU)Makefile provided, but it is primarily a test and missing vital end-user functionality such as tape/disk creation. Feel free to examine it and make any necessary changes or to suggest improvements.

##### Notes

  * **License:** A copy of the original "Cloak of Darkness" specification, including Roger Firth's copyright permissions, has been included from Jason Self's (formerly?) public git repository.

  * **Treaty of Babel:** I have assigned this "Cloak of Darkness" implementation an IFID as per §§2.2.4 of the Treaty and created an .iFiction file.
  
