# "Cloak of Darkness" Specification

Implementations should try be made as similar as possible. That is, things like object names and room descriptions should be identical, and the general flow of the game should be pretty comparable. Having said that, games are implemented using the native capabilities of the various systems, using features that a beginner might be expected to master; there shouldn't be any need to resort to assembler routines, library hacks, or other advanced techniques. The target is to write naturally and simply, while sticking as closely as possible to the goal of making the games directly equivalent.

"Cloak of Darkness" is not going to win prizes for its prose, imagination or subtlety. Or scope: it can be played to a successful conclusion in five or six moves, so it's not going to keep you guessing for long. (On the other hand, it may qualify as the most widely-available game in the history of the genre.) There are just three rooms and three objects.

  * The **Foyer** of the Opera House is where the game begins. This empty room has doors to the south and west, also an unusable exit to the north. There is nobody else around.
  * The **Bar** lies south of the **Foyer**, and is initially unlit. Trying to do anything other than return northwards results in a warning message about disturbing things in the dark.
  * On the wall of the **Cloakroom**, to the west of the **Foyer**, is fixed a small brass **hook**.
  * Taking an inventory of possessions reveals that the player is wearing a black velvet **cloak** which, upon examination, is found to be light-absorbent. The player can drop the **cloak** on the floor of the **Cloakroom** or, better, put it on the **hook**.
  * Returning to the **Bar** without the **cloak** reveals that the room is now lit. A **message** is scratched in the sawdust on the floor.
  * The **message** reads either "You have won" or "You have lost", depending on how much it was disturbed by the player while the room was dark.
  * The act of reading the **message** ends the game.

And that's all there is to it...

# Acknowledgements

Thanks to Stuart Allen, Mike Arnautov, Steve Breslin, Neil Cerutti, Al Golden, Stephen Griffiths, Mark Hughes, John Menichelli, Todd Nathan, Roger Plowman, Roddie Ramieson, Robin Rawson-Tetley, Dan Shiovitz, Kent Tessman, Alex Warren and Campbell Wild for contributing versions of "Cloak of Darkness". Not forgetting the special debt of gratitude we owe to those who created the authoring systems, without whom we'd probably still be adventuring in FORTRAN.

# Licensing

Copyright (C) Roger Firth

Copying and distribution, with or without modification, are permitted in any medium without royalty provided the copyright notice and this notice are preserved.
