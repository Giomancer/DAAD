#
#	GNUmakefile
#
#
# Not the best Makefile in the world. The "dist" target depends upon running "make && make dist"
# Haven't implemented making the tape/disk files yet.
#

# Game file; set NAME from command line
NAME = cloak
GAME = ./$(NAME)/$(NAME)

# Utils
PHP = php
AR = tar
# ZIP = lzip
ZIP = gzip

# DAAD
prefix = /mnt/d/IF
DAAD = $(prefix)/DAAD/DRC
BABEL = $(prefix)/babel
DRF = $(DAAD)/drf
DRB = $(DAAD)/drb.php
DM = $(DAAD)/daadmaker

# Files
files = \
$(GAME).blorb \
$(GAME).dsf \
./$(NAME)/README.md \
./$(NAME)/spec.md

.RECIPEPREFIX = >

# Parsing command line
ifeq ($(strip $(MAKECMDGOALS)),)
    SYS = zx esxdos
    TYPE = tape
    SAY = @echo "Creating DAAD database file:"
    .DEFAULT_GOAL := zx48
else ifeq ($(MAKECMDGOALS),c64)
    SYS = c64
    TYPE = SPLIT
    PFLAGS = -ch
    SAY = @echo "Creating Commodore 64 DAAD database file:"
else ifeq ($(MAKECMDGOALS),dos)
    SYS = pc vga256
    SAY = @echo "Creating MS-DOS DAAD database file:"
else ifeq ($(MAKECMDGOALS),msx)
    SYS = msx
    SAY = @echo "Creating MSX DAAD database file:"
else ifeq ($(MAKECMDGOALS),zx48)
    SYS = zx esxdos
    TYPE = tape
    SAY = @echo "Creating ZX Spectrum 48k DAAD database file:"
else ifeq ($(MAKECMDGOALS),zxnext)
    SYS = zx zxnext
    SAY = @echo "Creating ZX Spectrum Next DAAD database file:"
endif

# Recipe
define build-ddb
$(SAY)
$(DRF) $(SYS) $(GAME).dsf $(TYPE)
$(PHP) $(DRB) $(SYS) en $(GAME).json $(GAME).ddb $(PFLAGS)
@echo "Database complete."
endef

# Commodore 64
.PHONY: c64
c64 :
> $(build-ddb)

# MS-DOS
.PHONY: dos
dos :
> $(build-ddb)

# MSX
.PHONY: msx
msx :
> $(build-ddb)

# ZX Spectrum 48k
#   Canonical target
.PHONY: zx48
zx48 :
> $(build-ddb)

# ZX Spectrum Next
.PHONY: zxnext
zxnext :
> $(build-ddb)

.PHONY: blorb
blorb :
> @echo "Preparing blorb:"
> $(BABEL) -blorbs $(GAME).ddb $(GAME).ifiction
> @echo "Finished."

.PHONY: dist
dist : blorb
> @echo "Preparing distribution files:"
> $(AR) -cf $(NAME).tar $(files)
> $(ZIP) -f $(NAME).tar
> @echo "Finished."

.PHONY: clean
clean :
> rm $(GAME).ddb
> rm $(GAME).json

.PHONY: distclean
distclean : clean
> rm $(GAME).blorb
> rm ./$(NAME).tar*
